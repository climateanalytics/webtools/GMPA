﻿using Arlanet.Umbraco.Grid.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMPA.Core.Models.ViewModels
{
	public class HomeViewModel : MainViewModel
	{
        public BlockListGrid BlockListGrid { get; set; }
    }
}
