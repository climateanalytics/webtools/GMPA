﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMPA.Core.Models.ViewModels.GridViewModels
{
    public class HeaderViewModel
    {
        public string Title { get; set; }
    }
}
