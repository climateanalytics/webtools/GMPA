﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GMPA.Core.Models.ViewModels
{
	public class AboutTheGmpaViewModel : MainViewModel
	{
		public HtmlString BodyText { get; set; }
	}
}
