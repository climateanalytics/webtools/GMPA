﻿using System.Collections.Generic;

namespace Marketing.Core.Grid
{
    public class MyColumnSettings
    {
        public List<string> CssClasses { get; set; }
        public Alignment HorizontalAlignment { get; set; }
        public Alignment VerticalAlignment { get; set; }
    }
}
