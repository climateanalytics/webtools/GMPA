﻿namespace Marketing.Core.Grid
{
    public enum Alignment
    {
        None = 0,
        Start,
        Center,
        End
    }
}
