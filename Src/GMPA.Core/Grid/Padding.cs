﻿namespace Marketing.Core.Grid
{
    public enum Padding
    {
        None = 0,
        Single,
        Double, 
        Big
    }
}
