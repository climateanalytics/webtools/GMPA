﻿using Arlanet.Umbraco.Grid.Base;
using Marketing.Core.Models.Umbraco;
using Microsoft.AspNetCore.Html;

namespace Marketing.Core.Grid.Controls
{
    public class MarkdownGridControl : GridControl
    {
        public class MarkdownViewModel
        {
            public IHtmlContent Text { get; set; }
        }

        public override string Alias => "componentMarkdown";

        public override GridControlViewModel Render(BlockListGridControl gridControl, bool preview = false)
        {
            var component = (ComponentMarkdown)gridControl.Component;

            return ViewModel(ViewPath, new MarkdownViewModel
            {
                Text = component.Content
            });
        }
    }
}
