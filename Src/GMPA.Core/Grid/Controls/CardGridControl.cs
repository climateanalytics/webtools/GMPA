﻿using Arlanet.Umbraco.Grid.Base;
using Marketing.Core.Models.Umbraco;
using Marketing.Core.Models.ViewModels.Cards;
using Umbraco.Cms.Core.Mapping;

namespace Marketing.Core.Grid.Controls
{
    public class CardGridControl: GridControl
    {
        private readonly IUmbracoMapper _mapper;
        public override string Alias => "componentCard";
        public override string ViewPath => "~/Views/Partials/Card/Card.cshtml";

        public CardGridControl(IUmbracoMapper mapper)
        {
            _mapper = mapper;
        }

        public override GridControlViewModel Render(BlockListGridControl gridControl, bool preview = false)
        {
            var cardContent = (ComponentCard)gridControl.Component;

            return ViewModel(ViewPath, _mapper.Map<CardViewModel>(cardContent));
        }
    }
}
