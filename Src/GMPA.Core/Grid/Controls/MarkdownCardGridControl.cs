﻿using Arlanet.Umbraco.Grid.Base;
using Marketing.Core.Models.Umbraco;
using Marketing.Core.Models.ViewModels.Grid;
using Umbraco.Extensions;

namespace Marketing.Core.Grid.Controls
{
    public class MarkdownCardGridControl : GridControl
    {
        public override string Alias => "markdownCard";
        public override string ViewPath => "~/Views/Partials/MarkdownCard.cshtml";

        public override GridControlViewModel Render(BlockListGridControl gridControl, bool preview = false)
        {
            var component = (MarkdownCard)gridControl.Component;

            var markdownCardViewModel = new MarkdownCardViewModel()
            {
                Title = component.Title,
                Description = component.Text,
                ImageUrl = component.Image?.Url()
            };

            return ViewModel(ViewPath, markdownCardViewModel);
        }
    }
}
