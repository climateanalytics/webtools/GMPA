﻿using System.Collections.Generic;
using Arlanet.Umbraco.Grid.Base;
using Marketing.Core.Models.Media;
using Marketing.Core.Models.Umbraco;
using Marketing.Core.Models.ViewModels.Grid;

namespace Marketing.Core.Grid.Controls
{
    public class BrandSliderGridControl : GridControl
    {
        public override string ViewPath => "~/Views/Partials/BrandSlider.cshtml";
        public override string Alias => "componentBrandSlider";

        public override GridControlViewModel Render(BlockListGridControl gridControl, bool preview = false)
        {
            var component = (ComponentBrandSlider)gridControl.Component;

            var images = new List<ImageModel>();

            foreach (var mediaItem in component.BrandImages)
            {
                var image = mediaItem.Content as Image;

                var imageModel = new ImageModel(image, aspectRatio: AspectRatio.OneByOne);

                images.Add(imageModel);
            }

            return ViewModel(ViewPath, new BrandSliderViewModel
            {
                BrandImages = images, 
                Header = component.Header, 
                SubHeader = component.SubHeader
            });
        }
    }
}
